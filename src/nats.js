const stan = require("node-nats-streaming");

module.exports = function (mongoClient) {
  const conn = stan.connect("test-cluster", "test", {
    url: process.env.BROKER_URL,
  });

  conn.on("connect", () => {
    console.log("Connected to NATS Streaming");

    const opts = conn.subscriptionOptions().setStartWithLastReceived()
    const subscription = conn.subscribe('users', opts)
    subscription.on('message', (msg) => {
      console.log(msg)
    })

    setTimeout(() => {
      subscription.unsubscribe()
      subscription.on('unsubscribed', () => {
        conn.close()
      })
    }, 1000)

  })

  return conn;
};

