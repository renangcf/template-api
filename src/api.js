const jwt = require("jsonwebtoken");
const express = require("express");
const cors = require("cors");
const { v4: uuid } = require("uuid");


module.exports = function (corsOptions, { stanConn, mongoClient, secret }) {
  const api = express();
  const uid = uuid();
  const db = mongoClient.db('myDB');
  const dbusers = db.collection('Users')

  api.use(express.json());
  api.use(cors(corsOptions));

  api.get("/", (req, res) => res.json("Hello, World!"));

  api.post("/users",(req, res) =>  {

    if (req.body.name === null){res.status(400).send({"error": "Request body had missing field {name}"})}
    else if (req.body.email === null){res.status(400).send({"error": "Request body had missing field {email}"})}
    else if (req.body.password === null){res.status(400).send({"error": "Request body had missing field {password}"})}
    else if (req.body.passwordConfirmation === null) {res.status(400).send({"error": "Request body had missing field {passwordConfirmation}"})}
    else if (typeof req.body.name !== 'string'){res.status(400).send({"error": "Request body had malformed field {name}"})}
    else if (typeof req.body.email !== "string"){res.status(400).send({"error": "Request body had malformed field {email}"})}
    else if (typeof req.body.password !== "string") {res.status(400).send({"error": "Request body had malformed field {password}"})}
    else if (typeof req.body.passwordConfirmation !== "string") {res.status(400).send({"error": "Request body had malformed field {passwordConfirmation}"})}
    else if (req.body.password.length < 8 || req.body.password.length > 32){res.status(400).send({"error": "Request body had malformed field {Password}"})}
    else if (req.body.passwordConfirmation.length < 8 || req.body.passwordConfirmation.length > 32) {res.status(400).send({"error": "Request body had malformed field {passwordConfirmation}"})}
    else if (req.body.passwordConfirmation !== req.body.password){res.status(422).send({"error": "Password confirmation did not match"})}

    else {
      res.status(201).send({
        user: {
          id: uid,
          name: req.body.name,
          email: req.body.email
        }
      })
    }

    const UserCreated = ({
      eventType: "UserCreated",
        entityId: uid,
        entityAggregate: {
          name: req.body.name,
          email: req.body.email,
          password: req.body.password,
      }
    })

    stanConn.publish('users', UserCreated);
    dbusers.findOne({UserCreated: UserCreated.entityId});
    });




  api.delete(`/users/:uid`, (req, res) =>  {
    if(!req.get("Authentication")){res.status(401).send({"error": "Access Token not found"})}
    const token = req.get("Authentication")
    const tokenClean = token.slice(7)
    const decoded = jwt.verify(tokenClean, secret)
    if(req.params.uid !== decoded.id){res.status(403).send({"error": "Access Token did not match User ID"})}
    else{
      const UserDeleted = ({
        "eventType": "UserDeleted",
        "entityId": uid,
        "entityAggregate": {}
    })

      stanConn.publish('users', UserDeleted);
      res.status(200).send({"id": req.params.id})}
  });


  return api;

};
